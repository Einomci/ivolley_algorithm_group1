import sys
from pathlib import Path

from mmpose.apis import MMPoseInferencer
from src.models.body import Body
from src.models.common import DetectMultiBackend
from src.models.hand import Hand
from src.utils.torch_utils import select_device

# 获取当前文件的路径，并添加其父目录到系统路径中
FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))

video_path = "D:\\BUAA\\iVolley\\ivolley_algorithm_group1\\videos\\b.mp4"
# 初始化模型和设备
body_estimation = Body(str(ROOT / 'model/body_pose_model.pth'))
hand_estimation = Hand(ROOT / 'model/hand_pose_model.pth')
device = select_device()
volleyball_model = DetectMultiBackend(str(ROOT / 'model/yolov5x.pt'), device=device, dnn=False,
                                      data='data/coco128.yaml')
inferencer = MMPoseInferencer('wholebody')
result_generator = inferencer(video_path, show=True, pred_out_dir='../predictions', out_dir='../output')
# 将结果存储在列表中
results = []
for result in result_generator:
    results.append(result)
data = {'predictions': []}
for result in results:
    data['predictions'].append(result['predictions'])

print(data)
