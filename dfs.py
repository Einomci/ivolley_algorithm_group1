import os
import sys
from pathlib import Path

from mmpose.apis import MMPoseInferencer
from src.models.body import Body
from src.models.common import DetectMultiBackend
from src.models.hand import Hand
from src.utils.torch_utils import select_device

# 获取当前文件的路径，并添加其父目录到系统路径中
FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))

# 初始化模型和设备
body_estimation = Body(str(ROOT / 'model/body_pose_model.pth'))
hand_estimation = Hand(ROOT / 'model/hand_pose_model.pth')
device = select_device()
volleyball_model = DetectMultiBackend(str(ROOT / 'model/yolov5x.pt'), device=device, dnn=False,
                                      data='data/coco128.yaml')
inferencer = MMPoseInferencer('wholebody')


# 定义深度优先搜索函数
def dfs_video_files(folder_path):
    video_files = []
    for item in os.listdir(folder_path):
        item_path = os.path.join(folder_path, item)
        if os.path.isdir(item_path):
            # 如果是文件夹，则递归调用自身
            video_files.extend(dfs_video_files(item_path))
        elif item.endswith('.mp4') or item.endswith('.avi') or item.endswith('.mov'):
            # 如果是视频文件，则将其路径添加到 video_files 列表中
            video_files.append(item_path)
    return video_files


# 获取根目录下的所有视频文件路径
video_files = dfs_video_files(os.path.join(ROOT, ''))

from main import solve
# 对每个视频文件进行处理
for video_path in video_files:
    solve(video_path)

